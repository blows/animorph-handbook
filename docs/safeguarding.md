# Safeguarding policy

This Policy applies to all staff, including senior managmers and the board of trustees, paid staff, volunteers and sessional workers, agency staff, students or anyone working on behalf of Animorph Ltd.

## The purpose of this policy

- To protect children and young people who receive Animorph Ltd.’s services. This includes children of adults who use our services;
- To provide staff and volunteers with overeaching principles that guide or approach safeguarding and child protection;

Animorph Ltd. believes that a child or young person should never experience abuse of any kind. We have responcibility to promote the welfare of all children and young people and to keep them safe. We are committed to practice in a way that protects them.

## Legal framework

This policy has been drawn up on the basis of law and guidance that seeks to protect children, namely:

- Children Act 1989
- United Convention of the Rights of the Child 1991
- Data Protection Act 1998
- Human Rights Act 1998
- Sexual Offences Act 2003
- Children Act 2004
- Safeguarding Vulnerable Groups Act 2006
- Protection of Freedoms Act 2012
- Children and Families Act 2014
- Special educational needs and disability (SEND) code of practice: 0 to 25 years – Statutory guidance for organizations which work with and support children and young people who have special educational needs or disabilities; HM Government 2014
- Information sharing: Advice for practitioners providing safeguarding services to children, young people, parents and carers; HM Government 2015
- Working together to safeguarding children: a guide to inter-agency working to safeguard and promote the welfare of children; HM Government 2015

This policy should be read alongside our policies and procedures on:

- Recruitment, introduction and training
- Role of the designated safeguarding officer
- Dealing with disclosures and concerns about children or young person
- Managing allegations against staff and volunteers
- Recording and information sharing
- Code of conduct for staff and volunteers
- Safer recruitment
- E-safety
- Anti-bullying
- Complaints
- Whistleblowing
- Health and Safety
- Training, supervision and support
- Lone working policy and procedure
- Quality assurance

We recognize that;

- the welfare of the child is paramount, as enriched in the Children Act 1989
- all children regardless of age, disability , gender reassignment, race, religion or belief, sex or sexual orientation
- some children are additionally vulnerable of the impact of previous experiences, their level of dependency, communication needs or other issues
- working in partnership with children, young people, their parents, carers and other agencies is essential in promoting young people’s welfare.

We will seek to keep children and young people safe by:

- valuing them, listening to and respecting them
- appointing a Designated Safeguarding officer (DSO) for children and young people, a deputy and a lead board member for safeguarding
- adopting child protection and safeguarding practices through procedures and a code of conduct for staff and volunteers
- developing and implementing and effective e-safety policy and related procedures
- providing effective management for staff and volunteers through supervision, support, training and quality assurance measures
- recruiting staff and volunteers safely, ensuring all necessary checks are made
- recording and storing information professionally and securely, and sharing information about safeguarding and good practice with children, their families, staff and volunteers via leaflets, posters , one-to-one discussions
- using our safeguarding procedures to share concerns and relevant information with agencies who need to know, and involving children, young people, parents, families and carers appropriately.
- using our procedures to manage any allegations against staff and volunteers appropriately
- creating and maintaining an anti-bullying environment and ensuring that we have a policy and procedure to help us deal with any bullying that does arise
- ensuring that we have effective complaints and whistleblowing measures in place
- ensuring that we provide a safe physical environment for our children, young people, staff and volunteers, by applying health and safety measures in accordance with the law and regulatory guidance.

## Contact details

Designated Safeguarding Officer (DSO)Name: Szczepan Orlowski (Founder and Director)
Phone/email: sz@animorph.coop / 07429 964 619

## Senior lead of Safeguarding
Name: Geoffrey Morgan (Founder & Producer)
Phone/email: geoff@animorph.coop / 07508 580 516

CEOP
www.ceop.police.uk

NSPCC Helpline
0808 800 5000

We are committed to reviewing our policy and good practice annually.

The policy was last reviewed on:  01.05.18

Signed:   Geoffrey Morgan, Founder & Producer
