# Onboarding

Once the decision has been made to hire someone, we will need to on-board them into the cooperative. Here's a checklist to help us through that process.

## Gather the Required Employee Information

- Confirmation of their right to work in the UK by providing a scan/copy of work or residence visas.
- National Insurance number
- Full name (including middle name)
- Address and telephone number, mobile number
- Date of birth
- Country of citizenship
- City and country of birth

## Confirm Work and Remuneration Details

- Hours/days of work
- Pay (amount and payroll process)
- Bank details
- Annual leave and sick leave
- Ensure employment contract has been signed

## Animorph Cooperative Orientation

- History of the organisation
- Cooperative structure
- Pillars of culture
- Decision making
- Employment policy & process development
- Communications culture (online and offline)
- Conflict resolution process

## Tools

Introduction, access, and logins.

- Animorph Hub
    - Files
    - Calendar
    - Talk
    - Deck
    - Polls
- Email

## Person and Role Specific Orientation

- Introduction to steward and schedule first meeting
- Plan for welcome and introduction to team members
- Introduction to team processes
- Who will they work alongside while they get up to speed? Who should they go to with daily questions?
- Plan for their first set of tasks or projects
