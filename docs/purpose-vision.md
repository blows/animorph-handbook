# Purpose, vision, and values

## Purpose

Our purpose is to enrich the human potential of our workers, clients and audiences.

We believe that it is possible to continuously learn and improve the way people work while building higher-quality products that make positive contributions to the world.

## Vision

`TODO: Expand.`

guides and supports entrepreneurs, innovators, and enterprises in every phase of product innovation and enhancement.

## Values

`TODO: Expand.`

Our values describe how we work, what we represent, and guide us to be the kind of company and team members we want to be.

When we live up to these values we will:

- **Co-operation.**
- **Generosity.**
- **Independence.** Of thought and original perspectives. Doing what *we* think is right.
- **Transparency**. We prefer transparency of information whenever possible.
- **Assert Self-Management**. We are able to manage ourselves and don't need someone to tell us what to do in order to be productive. Our first instinct is to take initiative rather than expect a policy or ask for permission. We are able to make decisions on our own, to initiate change, and to take action independently. This doesn't mean we do everything alone. Rather, that we know when to collaborate, and when to ask for help.
- **Practice Continuous Improvement**

We recognise that we can always be better. We confidently go forward with the ideas we currently believe while remaining open and eager to better ideas. We take initiative to improve ourselves, the company, and our community.

This is a never-ending task, because there is always room for improvement. This week should be better than the last, and we should be optimistic that next week will be better than this week. We learn new things, and share those things with our peers and community.

While continually working to make things better, we prefer long-term viability over near-term shortcuts. It is more important that the organization exist in five years than it is that we make an extra dollar this year.

Earn, impart, and summon Trust

We expect the best from each other, give each other the benefit of the doubt, encourage each other to take initiative to improve ourselves and the company, and provide direct and constructive help to each other.

We show trust and respect for each other by telling the truth, and demonstrate honesty and integrity in our actions.

We avoid having private conversations about each other or clients. Instead, we use tools such as Basecamp, Google Meet, and GitHub to communicate openly within a project, within thoughtbot, and publicly.

We want to be able to choose our own tools and bring a respected voice to the way our projects are run. We do what we do not to take orders, but to collaborate and solve problems. We insist on working for clients who trust our role and experience.

We prefer transparency of information whenever possible.

We do all of this while maintaining the privacy and confidentiality that every person deserves.

Practice Continuous Improvement

We recognize that we can always be better. We confidently go forward with the ideas we currently believe while remaining open and eager to better ideas. We take initiative to improve ourselves, the company, and our community.

This is a never-ending task, because there is always room for improvement. This week should be better than the last, and we should be optimistic that next week will be better than this week. We learn new things, and share those things with our peers and community.

While continually working to make things better, we prefer long-term viability over near-term shortcuts. It is more important that the organization exist in five years than it is that we make an extra dollar this year.

Uphold Quality

We create working, maintainable, and understandable software that is enjoyable and easy to use. We improve the quality of the process and the client's environment. Beyond that, designing and building quality software means improving the security, privacy, and accessibility of the product; this requires improvements of life and reduction of harm for all users, contributors, and the people they affect.

Assert Self-Management

We are able to manage ourselves and don't need someone to tell us what to do in order to be productive. Our first instinct is to take initiative rather than expect a policy or ask for permission. We are able to make decisions on our own, to initiate change, and to take action independently. Taking action independently doesn't mean we do everything alone. Rather, that we know when to collaborate, and when to ask for help.

Seek Fulfillment in everything we do

We maintain an inclusive environment where we can thrive professionally and personally. We maintain a sustainable pace of productivity, and full lives outside of work. We maximize our ability to take on any project by creating a diverse team who can bring their experience and perspectives together to solve problems. We are proud of the work we are doing, and believe that it is meaningful, worth existing, and improves society and human well-being.
