# Collaboration tools

## Animorph Hub

We use an instance of Nextcloud which holds a suite of tools we use to co-ordinate our individual efforts. We use it to help us support, report, and operate the company.

You can access everything via a browser, and also sync to your desktop/smart phone. You can find information on how to set up syncing to your devices [here](https://hub.animorph.coop/f/19304).

### Files

[Files](https://hub.animorph.coop/apps/files) is where we store our collaborative documents.

### Calendar

[Calendar](https://hub.animorph.coop/apps/calendar/timeGridWeek/now) is our collective schedule, and where we log our hours.

The Animorph Main calendar is where we put events, meetings, and deadlines.

Each of us has an individual Hours calendar, visible to everyone, but editable only by you. We log the time we work as calendar events, named according to what we were doing.

### Talk

[Talk](https://hub.animorph.coop/apps/spreed/) is where we chat to each other.

### Deck

[Deck](https://hub.animorph.coop/apps/deck/) is our kanban-style organization tool used for action and project organisation.

### Polls

[Polls](https://hub.animorph.coop/apps/polls) is our polling tool used to gather availability for meetings.

## Email

We also talk to each other over email, and how we correspond with other externally.

## Git

We use [Git](https://www.git-scm.com/book/en/v2/Getting-Started-About-Version-Control) as a version control system for projects. It allows us to revert selected files back to a previous state, revert the entire project back to a previous state, compare changes over time, see who last modified something that might be causing a problem, who introduced an issue and when, and more. Generally it means that if we screw things up or lose files, we can easily recover, all this for very little overhead.

[git.coop](https://git.coop/) is the host for our Git repositories, and is a central point of collaboration on our code bases.

You can create an account using your Animorph email address. You can ask Szczepan or Hannah for a walkthrough.
