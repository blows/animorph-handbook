# Social value

## Policy

Our purpose is to enrich the human potential of our workers, clients and audiences. At Animorph we draw a line between being pragmatic and being cynical. These two tend to overlap. We believe both means and ends should go together.

Through our work we shall:

1) foster empathetic self-aware society,
2) empower users to constructively participate in public life,
3) address the challenges people face, be it social, medical or both,
4) build value-driven long-lasting relationships with clients,
5) nurture development of fringe ideas aiming to benefit society,
6) care for well-being and personal development of the co-operative's workers,
7) engage with burning issues (also known as wicked problems) such as income inequality, racial/ethnic discrimination or climate change.

The aims (outcomes) and objectives (activities) of Animorph:

Specific aims:
1. Improving recovery rates from physical and mental conditions
2. Boosting memory retention
3. Inciting empathy through participatory immersive experiences
4. Increasing practical understanding of immersive, wearable technologies

Objectives:
1. Apps treating mental conditions
2. Post-injury recovery apps
3. Cognitive-enhancement apps
4. Interactive documentaries and interactive works delivered with communities
5. Organising workshops and delivering courses

## Process

### Key Performance Indicator generator

[The Key Performance Indicator generator](https://hub.animorph.coop/f/6433) is what we use to score a project’s value against the Social Value Policy metrics.

#### Metrics

When assessing a potential project's viability and at the closing project retrospective, each of Social Value Policy points and some Additional Considerations are scored out of 10:

- Foster empathetic self-aware society
- Empower users to constructively participate in public life
- Address the challenges people face, be it social, medical or both
- Build value-driven long-lasting relationships with clients
- Nurture development of fringe ideas aiming to benefit society
- Care for well-being and personal development of the co-operative's workers
- Engage with burning issues (also known as wicked problems) such as income inequality, racial/ethnic discrimination or climate change
- Money
- Reputation
- Portfolio

The 7 Social Value Policy scores and 3 Additional Consideration scores are totalled to give a score out of 100.

## History

To embed our principles deeper into our practice, a resolution was passed on 19th March 2019 to amend our articles of association to legally hold ourselves accountable to the collection of Key Performance Indicators in our generator template, as guided by the Social Value Policy.

Two clauses were added to our Articles of Association:

### Accountability

110. Each quarter the coop members are obliged to produce a Key Performance Indicator report; which measures the social value provided by the co-ops operations. This report is drawn from metrics in the form of a social value policy established and approved unanimously by the co-op’s members; this policy must be reviewed annually at the AGM. The report must be circulated and reviewed at the next quarterly members meeting and the guidelines from the report must be considered and implemented. Once adopted, an annual summary of the year’s quarterly reports and the current social value policy must be published as a summary on the company’s website.

### Self Destruct Clause 🔥🔥🔥

111. If the co-op does not meet the social value policy for six consecutive quarterly reports 18months, a Special General Meeting must be called to rectify the co-op’s strategic operations, If the co-op does not address the persisting issues over the next two quarters, the company will wind up and the assets handed over to another organisation with the same or similar purposes. The recipient company is selected by majority vote and cannot have any current members as shareholders.
