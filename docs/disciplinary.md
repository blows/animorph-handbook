# DISCIPLINARY POLICY AND PROCEDURE

## STATEMENT OF POLICY

1. The aim of the ANIMORPH LTD. Disciplinary Policy is to help and encourage employees to improve, achieve and maintain standards of conduct, attendance and job performance.  It also enables management to deal effectively with those employees who do not comply with the Company standards of conduct, attendance and performance in the workplace.  Equally, the policy and procedure are designed in a manner which is non-discriminatory and which is fair, consistent and effective.  It must also be applied in a timely manner and without undue delay.

2. All Managers have a responsibility for ensuring that employees are made aware of the Disciplinary Policy and Procedure.  All employees are to be informed of the standards of conduct and work performance expected of them and Managers should ensure that these standards are fully understood by those who work to them.  Action taken under this policy must reflect fully the process detailed in the Procedural Appendix attached to this policy.

3. Matters relating to or arising under the Disciplinary Policy and Procedure must be treated as confidential at all times.  Failure to do so may itself constitute grounds for initiating disciplinary action.

## KEY PRINCIPLES

4. At each stage of the Disciplinary Procedure attached to this policy, the employee has a right to be accompanied by an accredited Trade Union representative or work colleague.  The employee will also be informed in writing of:

    - The nature of the complaint or allegation against them; and
    - The stage at which the matter is being considered.

5. The employee will also be reminded that they will be given a full opportunity to state their case and if action is taken, what improvement is required. They will also be reminded of their right of appeal.

6. Managers whose responsibilities require them to participate in or hold disciplinary meetings will be given appropriate training to enable them to undertake their role effectively and dispassionately.

7. Employees should make every effort to attend meetings or interviews relating to the application of the Disciplinary Procedure.  If an individual is unable to attend, they will need to give notice and the reasons why they are unable to attend.  The meeting will then be re-scheduled to a mutually convenient time.  Unless the reasons are exceptional, the re-arranged meeting must take place within 10 working days.  However, where an employee fails to attend such meetings more than once without compelling reasons, then meetings may be held in the employee’s absence.  Where this measure is invoked, the employee will be informed of this in writing.

8. Those responsible for making arrangements under the Disciplinary Policy and Procedure must ensure that any necessary, reasonable adjustments required by the employer or other attending have been addressed.  This may relate to disability or to the requirements of religious beliefs.

9. Should an employee have an objection to the person or persons appointed to investigate or hold meetings in connection with the disciplinary matter they must raise this objection in writing, clearly stating the reasons to Human Resources / Head Office.  Such objection must be made within two working days of the notification about the matter under investigation being brought to their attention.

10. The nature of the disciplinary action taken will be determined according to the nature and seriousness of the alleged misconduct and a dispassionate assessment of the facts based on the balance of probabilities.  Where misconduct is established and the sanction is a warning then subsequent misconduct within the currency of the warning may result in further and potentially more serious action which may ultimately result in dismissal.  However, no employee will be dismissed for a first instance of misconduct but summary dismissal may occur where gross misconduct is established.  Employees have a right of appeal against any disciplinary warning or sanction.

11. The Company’s Disciplinary Policy and Procedure will not apply to any employee who is in their period of probation.  Also, any proposed application of this policy to accredited Trade Union representatives must be the subject of prior consultation with Human Resources / senior management who will notify a senior full-time official of the Trade Union concerned.

12. Should an employee raise a complaint under the Company’s Grievance Policy, or any other related policy, whilst the subject of action under the Disciplinary Policy and Procedure and the complaint relates directly or indirectly to the matter under investigation, then action under the Disciplinary Policy will be adjourned whilst an urgent enquiry into the complaint is carried out.  If the grievance or complaint is rejected or found to have no bearing on the matter being investigated under the Disciplinary Policy, then the disciplinary proceedings will continue from the point at which they were adjourned.

13. Data relating to the application of this Policy and Procedure will be held and destroyed in accordance with the provisions of current data protection regulations (including the General Data Protection Regulations) and any Company policy which derives from those regulations.

14. In accordance with the current equality legislation, this procedure will not discriminate, either directly or indirectly, on the grounds of gender, race, colour, ethnic or national origin, sexual orientation, marital status, religion or belief, age, trade union membership, disability, offending background or any other personal characteristics.

15. This policy and procedure will be reviewed periodically giving due consideration to any legislative changes.

## APPENDIX

### DISCIPLINARY PROCEDURE

16. The purpose of the Disciplinary Procedure is the achievement of positive improvements by employees where shortcomings or failures are identified.  Any failure to attain required standards will be brought to the attention of the employee concerned at the earliest opportunity wherever possible via the informal procedure detailed below.  The attention of managers is, however, drawn to the Key Principles above where the employee whose performance or conduct has given rise to concern is an accredited Trade Union representative or is a new employee still within their probationary period.
Informal Procedure

17. This procedure should be used where the lapse in performance or conduct can reasonably be said to be minor and an isolated instance.  Such matters should be addressed promptly by managers by way of an informal advisory discussion.  The objective will be to ensure the employee recognises and accepts their shortcomings, offer encouragement and help to improve and secure a commitment to do so.  A note of the discussion should be made for reference purposes and there should be no recourse to the formal procedure.

18. Examples where the Informal Procedure may be appropriate include, infrequent lateness, carelessness, lack of effort, minor insensitive behaviour toward colleagues.  The use of the Informal Procedure is simply an aspect of normal day to day management.  The line manager should make it clear that if the required improvement does not take place, consideration will necessarily be given to the use of the Formal Procedure.
Formal Procedure

19. The Formal Procedure will be applied where an employee does not respond appropriately or adequately to informal action or the manager considers that the breach of conduct that is believed to have occurred is too serious to be dealt with informally.  Examples of behaviour that may constitute misconduct resulting in disciplinary action are set out at Annex A to this Appendix, as are examples of behaviour which may constitute gross misconduct.  In using the Formal Procedure and determining whether the employee has committed ‘misconduct’ or ‘gross misconduct’, the burden of proof required on a dispassionate and objective assessment of the facts is the balance of probability.

#### Investigation

20. Before disciplinary proceedings can take place, a wholly impartial investigation must be undertaken to collect information relating to the allegations and to determine whether the case should proceed to a disciplinary meeting.  An Investigating Officer will be appointed by the relevant line manager.  The Investigating Officer should where possible meet the following criteria:

    - Be wholly independent and have no connection whatsoever to the matter under investigation;
    - Be able to undertake the investigation promptly and unless there is particular complexity or non-availability of key interviewees, complete enquiries and provide a report within 15 working days, indicating what action, if any, should be considered.

21. Upon receipt of the Investigating Officer’s report, the relevant line manager must decide what action, if any, should be taken.  Three courses of action are available:

    - There is no case to answer.  In such circumstances, the employee concerned must be told immediately should that be the case;
    - That the matter can be resolved through guidance, counselling or further training;
    - That there is a case to answer and that a disciplinary meeting requires to be convened.

#### The Disciplinary Meeting (See also Annex B).

22. Before the disciplinary meeting the employee will be advised in writing of the purpose of the meeting and details of the complaint or allegation being considered, covering all issues to be discussed.  The individual will be given a minimum of 5 working days notice of the disciplinary meeting. If the individual’s representative or work colleague is not available to attend on the date proposed, the Company will endeavour to offer an alternative reasonable date within 5 working days of the original date. Note: This meeting will normally only be re-arranged once, except in exceptional circumstances.

23. Should either party wish to call any witnesses to the disciplinary meeting they must give at least 3 working days’ notice to the Disciplinary Panel, and have full responsibility for arranging the attendance of these witnesses.

24. All relevant facts and evidence will be made available to the employee at least 5 working days prior to the disciplinary meeting.  Additional information gathered by the employee that they wish to present at the meeting, must also be made available to the disciplinary panel at least 1 working day prior to the meeting.  Either party may present evidence including details of previous relevant warnings, witness statements, call witnesses and have the opportunity to ask questions.  An adjournment must be held in order that there can be a period of dispassionate reflection by the Disciplinary Panel to consider what action, if any, is to be taken.  Where possible, both parties will be verbally informed of the outcome after the adjournment.

25. The employee will be advised in writing of the outcome of the disciplinary meeting within 7 working days unless a longer period is specified and can be justified.  If disciplinary action is taken, the employee will be informed of the required improvements which are necessary and if applicable details of timescales for achievement, the duration of the warning and the consequence of a failure to improve performance as required.  The letter must include the date of the disciplinary meeting, the reason for issuing the warning as well as details of any sanctions which may be imposed.  It should also be noted whether the employee invoked their right to be accompanied. The right of appeal will also be included.

#### Warnings and Penalties

26. The outcome of the disciplinary meeting will generally fall into one of the following categories:

    - Case dismissed - no further action required
    - The employee is required to attend counselling or retraining
    - Verbal warning
    - First written warning
    - Final written warning
    - Dismissal

27. Other possible sanctions may involve demotion, transfer to another post or location, or the right to self-certificate sickness absence.

28. The above sanctions may be applied as follows:

29. Verbal Warning:  In cases of an initial or minor issue, the verbal warning is appropriate.

30. First Written Warning:  If the issue is more serious or if there is a still an active Verbal Warning in place and insufficient improvement has been made or further misconduct occurs, a First Written Warning will normally be issued.  A first written warning will normally be valid for 6 months from the date of the disciplinary meeting.

31. Final Written Warning:  If the issue is even more serious or if there is a still an active First Written Warning in place and insufficient improvement has been made or further misconduct occurs, a Final Written Warning will normally be issued.  A final written warning will normally be valid for 12 months from the date of the disciplinary meeting.  In exceptional cases validity may be longer.

32. Dismissal with Notice:  If within 12 months of the issue of a Final Written Warning further misconduct occurs or insufficient improvement has been made, the employee will normally be dismissed with notice.   The employee will be provided with written reasons for dismissal, the date on which the employment will terminate, their entitlement to pay, and the right of appeal.  ANIMORPH LTD. reserves the right to make a payment in lieu of notice.  

33. Summary Dismissal:  Where behaviour or misconduct is sufficiently serious to constitute gross misconduct, the employee will normally be summarily dismissed - i.e. without notice. The employee will be provided with written reasons for dismissal, the date on which the employment will terminate and the right of appeal.
Expiry of Warnings

34. A record of any disciplinary sanction will be placed on the employee’s personal file.  A sanction will be considered to be spent and the record removed from the file provided that the employee’s conduct has been considered to be satisfactory throughout the period following the imposition of the sanction.
Dismissal

35. The decision to dismiss an employee may only be taken by a person designated by ANIMORPH LTD. to have such authority or the instruction of a person so designated.  No dismissal may take place without consultation with and the involvement of a senior manager / Director / Business Owner.

36. Where dismissal occurs, whether with notice or summary, following the establishment of gross misconduct, the employee and their representative will be provided with a letter setting out the Company’s decision.  The letter, which must be sent to the employee by recorded delivery, must give details of those present at the disciplinary meeting, excluding witnesses, details of the allegation(s) and the evidence presented, the terms of the decision to dismiss and the reasons and the date on which the employment will terminate and if a dismissal with notice, the notice period.  In all cases, the right of appeal will be specified.
Appeals

37. Any employee who receives a disciplinary warning, other sanction or notice of dismissal has the right of appeal.  Appeals must be lodged in writing with Head Office (contact point to be specified in the letter of notification) within 10 working days of the date of the written notice of the sanction.  This period may be extended only in exceptional circumstances.  The notice of appeal must state the grounds of the appeal.

38. Appeals will be heard within 15 working days of the receipt of the notice of appeal by a more senior manager than the person taking the action at first instance. Where possible, all appeal panels will include a senior manager / representative of Human Resources.  Both parties to the appeal must provide a full written statement of the case including the grounds upon which the appeal is made/resisted together with copies of any documents to which reference will be made.  All documents and the details of witnesses, if any must be notified to all parties, 5 working days before the hearing takes place.  The Appeal Hearing will follow the procedure set out in Annex C to this Appendix.

39. The decision of the Appeal Hearing is final.

#### Criminal Offences

40. Disciplinary action should not be taken automatically against an employee because he/she has been arrested, charged or convicted of a criminal offence, as the matters may not be work-related and may have no relevance or impact within the workplace.   Each case must be carefully considered by managers according to the particular circumstances.

41. Disciplinary action may be considered in circumstances where, for example, the employee has been convicted and the nature of the conviction or sentence or both:

    - Impairs the business or reputation of ANIMORPH LTD. and/or;
    - Makes the individual unsuitable for continued employment given the nature of the business of the Company or the employee’s role;
    - Would be unacceptable to other employees.

42. Should disciplinary action be progressed, the normal investigative policy and procedure of investigation is to be followed to address the issue, as described in the Policy and Procedure.

43. Where an employee is unable to attend work because they are under arrest or remanded in custody, disciplinary action should not be commenced as the employee may ultimately be innocent.  The position should be addressed by consideration of special or unpaid leave until the position is clarified.
Suspension

44. Suspension is not in itself regarded as a disciplinary action and does not involve any prejudgement, or imply that any misconduct has taken place.  It is a neutral act to enable an investigation of the allegations made.  A short period of suspension with full pay may be helpful or necessary, although it should only be imposed after careful consideration.  It should also be kept under review and brought to an end as quickly as possible allowing for full investigation.

45. In cases of alleged gross misconduct or when other circumstances dictate that it is inappropriate for the employee to remain at the normal place of work, the relevant manager will consider whether suspension is appropriate in the circumstances.  Where it is found there is a case to answer, a formal disciplinary meeting will be convened.  Examples of circumstances that may warrant suspension include, fighting or violence between colleagues, alleged criminal offences or sensitive situations, e.g. alleged sexual assault.

46. An employee suspended from duty will be given written confirmation of the suspension and the reason for this action.  Whilst on suspension, an employee must remain contactable and must be available to attend for any investigation/disciplinary meeting during normal working hours, unless mutually agreed otherwise.  An employee who is suspended from duty shall, throughout the period of suspension, continue to receive full pay.

47. Whilst an employee is on suspension, they should not attempt to contact colleagues connected to the case with the exception of their Trade Union representative, work colleague (who is to accompany them at any subsequent meeting, if proven that there is a case to answer) or their line manager.


## ANNEX A
### MISCONDUCT LIKELY TO RESULT IN DISCIPLINARY ACTION

48. When conduct is unsatisfactory this is usually referred to as ‘misconduct’ and can vary in its degree of seriousness.  Where an incident is very serious it is known as ‘gross misconduct’, and one incident can be sufficient to warrant dismissal.  The following list illustrates behaviour likely to constitute misconduct and gross misconduct, but it is neither exclusive nor exhaustive and there may be other matters, which are sufficiently serious to warrant inclusion.
Misconduct

49. Misconduct is defined as behaviour which, in the view of ANIMORPH LTD., would not normally destroy the relationship of trust between employer and employee; however, the behaviour is serious enough to warrant action short of dismissal.  The following list illustrates conduct likely to amount to misconduct, but again this list is neither exclusive nor exhaustive:

    - absenteeism and lateness, e.g. frequent late arrival at work; failure to comply with requirements to notify sickness absence; unauthorised absence from the workplace;
    - dishonesty, e.g. making unauthorised private phone calls (either excessive or inappropriate), sending personal mail at the Company's expense;
    - refusal to obey a lawful and reasonable instruction of a manager or supervisor;
    - failure to carry out the normal duties of the post;
    - unauthorised copying of copyright or licensed material, e.g. software;
    - unreasonable or unacceptable conduct, e.g. abusive language or behaviour;
    - misuse of facilities, loss, damage or misuse of Company property or equipment through wilfulness, negligence or carelessness;
    - threatening violence whilst at work to a colleague, service user or provider or members of the public;
    - breach of Company regulations, e.g. financial regulations, health and safety, confidentiality of personal records;
    - accepting significant gifts (i.e. a series of presents) or hospitality from contractors, service users, service providers or members of the public without authorisation;
    - driving whilst using a mobile phone;
    - incapability as a result of being under the influence of alcohol or illegal drugs at work;
    - incurring any motoring offences whilst on ANIMORPH LTD. business;
    - abuse of the email, social media and/or other online systems.

50. The distinction between misconduct and gross misconduct is often a matter of degree and some of the examples under misconduct may be of such an extreme nature that they amount to gross misconduct.  Alternatively, there may be instances when examples demonstrated under Gross Misconduct, depending upon all of the circumstances, may amount to misconduct.

#### Gross Misconduct

51. Gross misconduct is defined as behaviour, which in the view of ANIMORPH LTD. fundamentally destroys the trust between employer and employee and thereby warrants immediate dismissal.  The following list illustrates conduct likely to amount to gross misconduct, but this list is neither exclusive nor exhaustive:

    - theft or misappropriation or malicious damage to property of the Company, fellow employees, service users or providers;
    - falsifying records or expenses claims which result in gain to the individual, e.g. registers, time sheets, car expenses, overtime, flexitime, sickness claims;
    - physical violence towards colleagues, service users, service providers or other members of the public;
    - serious incapability as a result of being under the influence of alcohol or illegal drugs at work;
    - discriminatory behaviour relating to sexual orientation, race, gender, disability, religion or belief against other employees, service users and providers or members of the public. ANIMORPH LTD. operates a zero-tolerance approach;
    - fraudulent or false claims of harassment or victimisation;
    - Bribing or attempting to bribe another individual, or personally taking or knowingly allowing another person to take a bribe;
    - serious breach of Company regulations, e.g. financial regulations;
    - serious negligence which causes substantial loss, damage or injury;
    - non-compliance with health and safety rules and regulations where it endangers the well-being of the employee or others;
    - unauthorised use of Company vehicles, materials, equipment, facilities or labour for private purposes and/or personal gain;
    - unauthorised deliberate access to information held by the Company whether held on electronic or manual systems;
    - unauthorised disclosure of information classified as confidential by the Company;
    - falsification or omission of information for personal gain, for example, on an application form, medical questionnaire, etc.;
    - downloading inappropriate information from the internet, e.g. pornography;
    - victimisation or bullying (either in person or via email, etc.); and
    - improper use of position as a ANIMORPH LTD. employee for personal gain.

## ANNEX B

### CONDUCTING A DISCIPLINARY MEETING

52. A disciplinary meeting will normally be held by a panel consisting of a manager, who has not been previously involved in the matter, who will act as the Panel Chair.  They will where possible be accompanied by another appropriate manager / representative of Human Resources.

53. The Disciplinary meeting follow the following stages:

    - Opening the meeting by Panel Chair
    - Summary of allegation by the investigating officer, including calling of any witnesses
    - Employee, then the Disciplinary Panel, will have the opportunity to ask questions
    - Employee to present their answer to the allegations including calling of any witnesses
    - Opportunity to ask questions
    - Consideration of the facts
    - Opportunity for employee to make a final statement
    - Adjournment
    - Action to be taken (if any)
    - Establishment of a review date (if appropriate)

#### Opening the Disciplinary meeting

54. All employees are entitled to be accompanied by their Trade Union representative or a work colleague.  Where an employee is not accompanied, the employee must be reminded of this right, and if declined, this must be recorded.  

55. Those ‘hearing’ the disciplinary must introduce those present and outline the reasons for the disciplinary meeting taking place (the reason/s outlined in the invite to disciplinary letter) and the format the meeting will take.

#### Summary of Allegations

56. At this stage the investigating officer(s) should summarise their findings.  The investigating officer(s) should adhere to the facts and not introduce opinions, hearsay or issues that have not previously been mentioned.  All documentation that will be used as evidence (including previous relevant warnings and witness statements where applicable) will already have been made available to the individual prior to the disciplinary meeting taking place (copies will have been sent with the invite to disciplinary meeting letter).  

57. Should a new matter arise during the course of the disciplinary meeting then the Disciplinary Panel should adjourn in order that consideration may be given to the appropriateness of the introduction of this new matter. To avoid unnecessary duplication of the process as well as ensuring fairness, it may be more beneficial to adjourn the disciplinary meeting in order that further investigations may be carried out in relation to the new matter.

58. The aim of the disciplinary meeting is to seek verification and clarification about the issues of concern, through questions.  Where it is appropriate to call witnesses, either party may call and question them.

59. After the investigating officer has stated the outcome of their investigation the employee will be given the opportunity to ask questions and respond. The employee’s representative will also be able to ask questions for clarification purposes.

60. If the disciplinary meeting is dealing with multiple issues, each issue should be addressed in turn and the employee and/or their representative be allowed to respond in relation to each issue as it is addressed.

61. The investigating officer may remain present during the disciplinary meeting to allow for any questions.

#### Adjournment

62. Before any decision is taken, it is necessary to adjourn the disciplinary meeting to give adequate consideration to the facts as they have been presented and the responses that have been given to the allegations, including any mitigating circumstances.  At this stage all parties will be asked to leave the room and the panel must decide the facts of the case, with advice from Human Resources, where appropriate, and whether the behaviour requires disciplinary action to be taken and if so, at what level.

63. The disciplinary meeting may also be adjourned to consider other issues, e.g. to direct further investigations to take place or to investigate new information/facts that have been brought to light.

64. There is no set time for an adjournment and adjournments can be called at any time during the disciplinary meeting, by either party.

65. Taking disciplinary action is not a matter to be taken lightly and should only be taken if it is to be constructive in attempting to produce the desired behaviour.  Managers will also need to consider, if disciplinary action is to be taken, whether any other sanctions will be attached to the warning.

#### Action

66. When the disciplinary meeting is reconvened the Panel Chair should explain that consideration has been given to all of the issues raised at the beginning of the meeting, and all of the facts and issues raised during the course of the meeting.  The Panel Chair must then outline what action, if any, will be taken including any sanctions.

67. It is important that where a warning/sanction is given, the employee is informed of the length of time it will remain on their record, their right of appeal, the procedure that will be followed in relation to confirming the action in writing and any arrangements for the review of sanctions imposed.

#### Disciplinary Meeting Notes

68. The notes of the meeting will be held on file in Human Resources / Head Office.  All of the documentation in relation to a Disciplinary meeting will be marked confidential.

## ANNEX C
### APPEALS PROCEDURE

69. Appeals will normally be heard by a more senior manager to the person taking the first instance disciplinary action.  An employee may choose to appeal if, for example:

    - They think a finding or penalty is unfair
    - New evidence comes to light
    - They think the Disciplinary Procedure was not used correctly

70. An appeal may be heard as a paper-based exercise where the manager hearing the appeal will consider the circumstances of the case and the details of the employee’s appeal (which must be submitted in writing). However, the employee may request an Appeal Hearing to present their case in person.

71. The procedure for an Appeal Hearing is as follows:

    - The manager hearing the appeal will arrange a suitable date and venue
    - The manager will outline the circumstances of the disciplinary action first, explaining the reasons for the actions that have been taken.
    - The employee will then be able to explain the reasons for their appeal
    - The manager may then wish to ask the appellant any questions about their appeal case.
    - There will then be an adjournment while the manager considers the information they have heard and reach their decision.

72. The decision of the panel will be communicated to the employee verbally, wherever possible, and in any case will be confirmed in writing no later than 5 working days after the Appeal Hearing date.

## ANNEX D

### SUSPENSION LETTER (D1)

Dear

IN STRICTEST CONFIDENCE - SUSPENSION FROM DUTY

Further to our meeting held on (date) this letter is to confirm that you are suspended from duty with effect from (date).

You are suspended as a result of the following alleged incident/allegation made against you to allow a full investigation to take place.

(Details of the allegation/incident to be included here)

You are suspended subject to the following conditions:

The suspension will be for as short a time as possible.  Should your period of suspension extend beyond 4 weeks, we will advise you of this fact and give you an indication of the likely length of your suspension.

Suspension does not constitute disciplinary action.

Suspension will be on full pay (including contractual allowances).

You are obliged to keep us informed of your whereabouts during your suspension so that we can contact you to assist any investigations or to advise you of the next steps.  You are obliged to make yourself available to attend any meetings during your normal working hours.

At the end of your suspension, you will be sent a letter, either advising you to return to work on a specific date or to inform you of the arrangements for a disciplinary meeting.

During the period of suspension, you are expressly forbidden from entering any Company premises or contacting people at work unless to someone you have identified as your representative (a trade union official or named work colleague).

Any queries on this suspension should be made to <name of contact> on (Telephone No   xxxx).

Yours sincerely

(Manager)

### EXTENSION OF SUSPENSION LETTER (D2)

IN STRICTEST CONFIDENCE

Dear

NOTICE OF EXTENSION OF SUSPENSION FROM DUTY

Further to my letter of (date) and following our discussion on (date), I am writing to inform you that the initial 4-week period of suspension, due to end on (date) will be extended.  The extension to your period of suspension is due to the fact that investigations are still being carried out.

This extension to your suspension will be for a further 4-week period in the first instance, and is due to end on (date).  Should I need to extend your suspension again, I will notify you of that fact prior to (date).

This further period of suspension is subject to the conditions outlined in my letter of (date).

Yours sincerely

(Manager)

### NOTIFICATION OF DISCIPLINARY MEETING (D3)

IN STRICTEST CONFIDENCE

Dear

NOTIFICATION OF DISCIPLINARY MEETING

Further to (include details of the incident including the date) you are required to attend a disciplinary meeting on (date time venue).

The disciplinary meeting will be conducted by (name and title of manager) accompanied by (name and title of accompanying manager and/or Human Resources representative).  Also in attendance will be (names of investigating manager and any witnesses should be included here).

You are advised of your right to be accompanied by your union representative or a work colleague if you so wish.

I enclose the documents which will be referred to at this meeting.

If you have any documentation that you wish to put forward as part of the investigation, please provide this at least 1 working day prior to the meeting.  Please also provide details of any witnesses you wish to call to the meeting.

I would be grateful if you would contact me to confirm that you will be attending.

On arrival please report to (reception/a particular individual).

Yours sincerely

(Manager)


### NO FURTHER ACTION REQUIRED (D4)

IN STRICTEST CONFIDENCE

Dear

At your disciplinary meeting held on (date), we considered the facts surrounding (give details of the incident).

Having considered all the relevant information relating to this matter, I can confirm the decision made verbally to you on (date) that no action is to be taken.

Yours sincerely

(Manager)

### VERBAL WARNING (D5)

IN STRICTEST CONFIDENCE

Dear

VERBAL WARNING

Further to the disciplinary meeting held on (date), I am writing to confirm the issue of a verbal warning.

The warning was issued (date), following (describe the incident)

This warning will remain active for a period of 6 months from the date of the meeting.  Should you be found to repeat the type of behaviour leading to this incident during this 6 month period, you are warned that further disciplinary action, including dismissal, could be taken, for recurring problems.

Should there be no repeated or similar actions within the next 6 months, the warning will be made void and removed from your personnel file.

You have the right of appeal against this verbal warning.  If you wish to exercise this right, you should write to XXXXXX (address to be given) detailing the grounds of you appeal within 10 working days from the receipt of this letter.

Yours sincerely

(Manager)

### FIRST WRITTEN WARNING (D6)

IN STRICTEST CONFIDENCE

Dear

FIRST WRITTEN WARNING

Further to the disciplinary meeting held on (date), I am writing to confirm the issue of a first written warning.  The warning was issued (date), following (describe the incident).

This warning will remain active for a period of 12 months from the date of the meeting.  Should you be found to repeat the type of behaviour leading to this incident during this 12 month period, you are warned that further disciplinary action, including dismissal, could be taken, for recurring problems.  

Should there be no repeated or similar actions within the next 12 months, the warning will be made void and removed from your personnel file.

You have the right of appeal against this first written warning.  If you wish to exercise this right, you should write to XXXXXX (state address), detailing the grounds of you appeal within 10 working days from the receipt of this letter.

Yours sincerely

(Manager)

### FINAL WRITTEN WARNING (D7)

IN STRICTEST CONFIDENCE

Dear

FINAL WRITTEN WARNING

Further to the disciplinary meeting held on (date), I am writing to confirm the issue of a final written warning.  The warning was issued (date) following (describe the incident).

This warning will remain active for a period of 12 months from the date of the meeting.  Should you be found to repeat the type of behaviour leading to this incident during this 12 month period, you are warned that further disciplinary action, including dismissal, could be taken, for recurring problems.  

Should there be no repeated or similar actions within the next 12 months, the warning will be made void and removed from the personnel file.

You have the right of appeal against this final written warning.  If you wish to exercise this right, you should write to XXXXXX (state address), detailing the grounds for you appeal within 10 working days from the receipt of this letter.

Yours sincerely

(Manager)

### DISMISSAL (D8)

IN STRICTEST CONFIDENCE

Dear

DISMISSAL

Further to the disciplinary meeting held on (date) at which the panel considered (details).

I am writing to confirm the decision to dismiss you from your post of (post title) with effect from (date of the meeting).

You are entitled to (number) week(s) pay in lieu of notice.  This will be paid at full pay together with payment for any annual leave to which you are entitled up to (date).  You will also receive your P45.  

You have the right of appeal against this dismissal.  If you wish to exercise this right, you should write to XXXXXX (state address) detailing the grounds of your appeal within 10 working days from the receipt of this letter.

Yours sincerely

(Manager)

### SUMMARY DISMISSAL (D9)

IN STRICTEST CONFIDENCE

Dear

Further to the disciplinary meeting held on (date) at which the panel considered (describe)

I am writing to confirm the panel’s decision to summarily dismiss you from your post (post title) with effect from the date of the meeting.

As this is a summary dismissal, you are not entitled to notice or payment in lieu of notice.  However, you will be made a payment for any untaken annual leave due to you and this will be forwarded in due course, together with your P45.

You have the right of appeal against this dismissal.  If you wish to exercise this right, you should write to XXXXXX, (state address) detailing the grounds of you appeal within 10 working days from receipt of this letter.

Yours sincerely

(Manager)
