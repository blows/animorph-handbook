`TODO: Expand.`

A person that possesses our desired character strengths, as also defined in the playbook: good communicator, demonstrates initiative, motivated, enthusiastic, focused, composed, curious, optimistic, displays grit, emotionally intelligent, and able to feel gratitude.
