# GRIEVANCE PROCEDURE

1. The grievance procedure is intended as the tool by which a member of staff may formally have a grievance, regarding any condition of their employment, heard by the management of the Company. The aggrieved employee has the right to representation by a Trade Union Representative or a work colleague

2. In the event of a member of staff wishing to raise a grievance, it is preferable for the grievance to be satisfactorily resolved as close to the individual and their line manager as possible. It is understood however that this is not always possible and that a formal procedure is required to ensure the swift and fair resolution of matters which aggrieve ANIMORPH LTD. employees.

3. Time scales have been fixed to ensure that grievances are dealt with quickly, however these may be extended by agreement.
4. This procedure is not intended to deal with:
    - Dismissal or disciplinary matters which are dealt with in a separate procedure.
    - Disputes, which are of a collective nature and which are dealt with in a separate procedure.

## Stages of the Procedure

### Stage 1

5. An employee who has a grievance, should raise the matter with his line manager / supervisor immediately either verbally or in writing. If the matter itself concerns the employee’s immediate manager, then the grievance should be taken to their superior.

6. If the manager is unable to resolve the matter at that time then a formal written grievance form should be submitted (see appendix 1). The manager should then respond within 2 working days (i.e. the managers normal working days) to the grievance unless an extended period of time is agreed upon by both parties. The response will give a full written explanation of the mangers decision and who to appeal to if still aggrieved.

### Stage 2

7. In most instances, the Company would expect the manager’s decision to be final and for the matter to come to a close. However, in some circumstances the employee may remain aggrieved and can appeal against the decision of the manager concerned.

8. The appeal, to the manager next in line, must be made within ten working days of the original response to the employee’s grievance. The appeal must be in writing (see appendix 2) and contain the original formal Grievance form. This manager will attempt to resolve the grievance. A formal response and full explanation will be give in writing, as will the name of the person to whom they can appeal if still aggrieved, within 7 days.

9. Where the 'next in line' manager at this stage is the Director with responsibility for the employee’s function, then the grievance should immediately progress to stage 3.

### Stage 3

10. If the employee remains aggrieved there will be a final level of appeal to the Director responsible for the employee’s function. This appeal must be made in writing (see appendix 3), enclosing a copy of the original Formal Grievance form, to the Director within ten working days of receipt of the Stage 2 response. This Director will arrange and hear the appeal with, where possible, another management representative and respond formally with a full explanation within 20 working days.

11. Where a grievance is raised against a Director then the grievance will be heard by the Chief Executive / Business Owner

12. There is no further right of appeal. Where however both parties agree that there would be some merit in referring the matter to a third party for advice, conciliation or arbitration, arrangements will then be made to find a mutually acceptable third party.

#### Using mediation

13. An independent third party or mediator can sometimes help resolve grievance issues before it is necessary to invoke the formal procedure. Mediation is a voluntary process where the mediator helps two or more people in dispute to attempt to reach an agreement. Any agreement comes from those in dispute, not from the mediator. The mediator is not there to judge, to say one person is right and the other wrong, or to tell those involved in the mediation what they should do. The mediator is in charge of the process of seeking to resolve the problem but not the outcome.

14. There are no hard-and-fast rules for when mediation is appropriate but it can be used:

    - for conflict involving colleagues of a similar job or grade, or between a line manager and their staff
    - at any stage in the conflict as long as any ongoing formal procedures are put in abeyance
    - to rebuild relationships after a formal dispute has been resolved
    - to address a range of issues, including relationship breakdown, personality clashes, communication problems and bullying and harassment.

15. Mediation is not part of ANIMORPH LTD.’s formal grievance procedure. However, if both parties agree to mediation, then the grievance procedure can be suspended in an attempt to resolve the grievance through that route. If mediation is not successful, then the grievance procedure can be re-commenced.

## Appendix 1

### Notification of a Formal Grievance

To:

From:

Dept:

Date:

Immediate Superior:

Dear

I wish to take a formal grievance out against:

in line with the Company Grievance Procedure. The details of my grievance are shown below:

Yours sincerely,

(Manager should respond to this formal written grievance within 2 working days unless an extended period for response is mutually agreed)

## Appendix 2

### Notification of a Stage 2 Grievance

To:

From:

Dept:

Date:

Immediate Superior:

Dear

On (within 10 days of the response to the initial formal grievance) my grievance against was heard by

I am not satisfied with the outcome of this meeting and would like to appeal to yourself for a further hearing of my grievance, in line with the Company Grievance Procedure.

I enclose a copy of the original letter regarding this matter and other correspondence and information related to it.

Yours sincerely

(Manager should respond to this formal written grievance within 7 days unless an extended period for response is mutually agreed)

## Appendix 3

### Notification of a Stage 3 Grievance

To (Director):

From:

Dept:

Date:

Immediate Superior:

Dear

On (within 10 days of the response to the second stage of the formal grievance) I appealed to against the decision made at my initial grievance against:

I remain dissatisfied with the outcome of this meeting and would like to appeal to you for a further hearing of my grievance, in line with the Company Grievance Procedure.

I enclose a copy of the original letter regarding this matter and other correspondence and information related to it.

Yours sincerely

(Director should respond to this formal written grievance within 20 working days unless an extended period for response is mutually agreed)
