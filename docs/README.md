# Animorph Handbook

This is the central resource for how we work and run our company together.

The primary audience is Animorph members and collaborators, but is public for anyone who may find it useful — just as we have learned lots from [companies who've shared theirs](https://community.coops.tech/t/examples-of-governing-documents-handbooks-constitutions/101). The goal is to give us context and clarity internally, and share the way we work with others externally.

This handbook is a live set of documents, always under view with the potential to change. If you see something that could be improved you are warmly invited to improve it! Find out more about editing the handbook by talking to Hannah.
