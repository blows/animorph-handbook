# Leave and time off

## Illness, injury, and absence

### Absence policy

There'll be times when we can't fulfil work commitments due to illness, injury, or because something unexpected happened. We want to support each other through these times, to be able to continue working together. We will review and manage absence to minimise the impact on Animorph as a business.

### Short-term illness absence process

`TODO: Write up the process.`

### Ongoing illness absence process

`TODO: Write up the process.`

---

## Holiday

### Policy

### Process

- Check the Animorph Main calendar.
- Add the day/s you'll be away as a single all-day event.

### Paid Leave

#### Process

- Log in to Brightpay, and navigate to the 'Request Leave' area on the Calendar page where you can see the hours of annual leave you have remaining.
- Add the start and end date of the leave you'd like to request, the amount of hours per day, and any additional information, then submit your request.
- The request will be processed by Geoff, who will approve your leave and... `TODO: Hannah speak to Geoff about how this works on his end. Also how to assign people's work days...`
- The hours will be added to your next payslip.

## Maternity, paternity, adoption and parental leave provisions

```
TODO: Write up an employee viewpoint of statutory requirements
https://www.gov.uk/employers-maternity-pay-leave
```
