# Expenses and allowances

## Proposing a spend

Write a clear proposal as a card in the [Actions Deck](https://hub.animorph.coop/apps/deck/#/board/2), tag it with 'Proposal', and bring it to a General Meeting to be discussed. Include your reasoning and research to give others the context to make an informed decision.

### Equipment

As employees we are provided with the equipment we need in order to deliver our work for Animorph. This includes but it is not exclusive to:

- laptop or desktop computer
- office chair
- desk
- monitor
- keyboard
- mouse
- microphone
- webcam
- headphones

Conditions:

- Each member has an allowance of £500 per year to put towards the listed equipment, excluding computers.
- Members can propose for Animorph to buy a laptop or desktop computer for their use no more than once every 3 years. Have to have been employed by Animorph for a minimum of 1 year.
- If the computer is used for commercial work in addition to work for Animorph, you can claim a percentage of the total cost. The percentage will be calculated as the total number of hours worked in the previous year (ending date the last day of the month before the claim is submitted) divided by the number of hours of a full-time worker, 1440 (30hrs/week * 48weeks). If you leave the company within 1 year of being reimbursed for the expense, the amount will be deducted from your final payslip.

## Approved expenses

The following cases do not need to be brought to a General Meeting.

### Project-specific expenses

If the item was written into the budget of a project, then before making the purchase, approve the spend in writing with at least one member of the co-op also working on the project before submitting a claim for the expense.

### Lunch

Animorph pays for your lunch hour if you work 6 hours or more in a single day; simply log an additional 1 hour event labelled 'Lunch' in your personal hours calendar and this will be included automatically in your monthly pay.

### Books, music, research, and learning materials

We encourage learning, research, and personal development. If there is a book, game, course, or other item that would benefit members of Animorph and relates to our work, approve the spend in writing with at least one member of the co-op before submitting a claim for the expense.

We have a shared Bandcamp account, and each employee has an allowance of 1 album per month. Each month add an album to the Wish List, and one person will buy them all. We aim to make the purchase on Bandcamp Friday, so the full amount goes to the artists. Ask Michal if you have any questions.

## Reclaiming a spend

Upload an invoice, complete with receipts, to the [Invoices To Be Paid](https://hub.animorph.coop/f/5775) folder. The invoice will be paid within 30 days.
