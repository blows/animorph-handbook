`TODO: Expand.`

We've learned a ton from blog posts, tweets, and newsletters from others in the community. We try to always give back.

Blog

Give it a name?

Our blog is called Giant Robots Smashing Into Other Giant Robots.

We track and coordinate our blog post authoring on an Editorial Calendar Trello board:

When someone wants to write a post, they write its headline as a Trello card in the "Next Up" list of the board, and assign the card to their Trello user.

Spend time writing and re-writing a great headline. It helps narrow focus, figure out the purpose of the post, and grab people's attention in the first place.

When we begin writing, we move the Trello card to the "Drafts" list.

We write posts in Markdown, and store them in our blog's GitHub repo. We add tags to the post, which help our readers find related blog posts.

When we're ready for feedback from the team, we move the card to an "In Review" list and share the Trello card's URL with the team in Slack.

When the post is ready to publish, we give it a publication date, merge, and deploy.

Our RSS feed, Zapier, and Buffer accounts are set up to automatically work together to link to the post from Twitter, Facebook, Google+, and LinkedIn.

We also link to the post from Hacker News, Reddit, Delicious, Pinboard, or other appropriate sites.

Finally, we move the Trello card to the "Live" column.

Twitter

Everyone on the team has access to our thoughtbot Twitter account and can tweet at any time. If the tweet is not time-sensitive, we use Buffer to queue up tweets and keep a schedule.

We try to be conversational, casual, and real on our Twitter account. We talk the same way as we would in person among ourselves and be good-humored. Puns are encouraged. We aim to keep the quality high and for every tweet to be a hit. We want to avoid spelling mistakes, and use proper punctuation. We should respect the people who follow us.

To reach the largest audience, don't begin a tweet with a Twitter username.

Some tweet ideas include announcing meetups, open source releases, enthusiasm about a new tool or technique, tips on Git, Unix, and Vim, links to our blog posts, links to others' blog posts if they are excellent and not on the current Hacker News or Twitter cycle at the moment, and Funkmaster Flex.
