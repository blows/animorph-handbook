`TODO: Expand.`

The purpose of this guide is to ensure that consistency is maintained across the thoughtbot brand, whether assets are used internally or externally. Please reference this guide whenever you use these assets to ensure that they are used correctly.

    Logo - Including the logomark and logotype
    Colors - Brand colour palette
    Typography - Typefaces
    Voice & Tone - Brand Personality, Tone & Voice
    Photography - Studio Images
    Icons and Graphics - Icon, Hand Drawn Elements, Textures, Other Graphics
    Media Advertisement - Video Content, Social Media Posts, Email, Signage

Tagline

    Let's make your product and team a success.

Tagline use

The tagline communicates what we do and the value we provide in a few words. It is used to succinctly describe who we are to the reader. The tagline is an external-facing representation of our positioning.

The thoughtbot tagline should be kept up to date wherever it lives: * thoughtbot.com homepage * sales and marketing collateral * Online listings like * Twitter * LinkedIn * Facebook * Instagram * GitHub * Dribbble * Clutch
